﻿using Grpc.Core;
using Grpc.Core.Interceptors;
using Microsoft.Extensions.Logging;

namespace SimpleInterceptorClient.Interceptors;

public class ClientLoggerInterceptor : Interceptor
{
    private readonly ILogger logger;

    public ClientLoggerInterceptor(ILoggerFactory loggerFactory)
    {
        logger = loggerFactory.CreateLogger<ClientLoggerInterceptor>();
    }

    public override AsyncUnaryCall<TResponse> AsyncUnaryCall<TRequest, TResponse>(
        TRequest request,
        ClientInterceptorContext<TRequest, TResponse> context, Interceptor.AsyncUnaryCallContinuation<TRequest, TResponse> continuation)
    {
        var call = continuation(request, context);

        return new AsyncUnaryCall<TResponse>(
            HandleResponse(call.ResponseAsync),
            call.ResponseHeadersAsync,
            call.GetStatus,
            call.GetTrailers,
            call.Dispose);
    }

    private async Task<TResponse> HandleResponse<TResponse>(Task<TResponse> inner)
    {
        try
        {
            logger.LogInformation($"Sending Request.");
            return await inner;
        }
        catch (Exception ex)
        {
            logger.LogError(ex, $"Error thrown");
            throw new InvalidOperationException("Custom error", ex);
        }
        finally
        {
            logger.LogInformation($"Request Completed.");
        }
    }
}