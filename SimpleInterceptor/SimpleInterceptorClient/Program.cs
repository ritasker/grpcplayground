﻿using Grpc.Core.Interceptors;
using Grpc.Net.Client;
using Microsoft.Extensions.Logging;
using SimpleInterceptorClient;
using SimpleInterceptorClient.Interceptors;

var loggerFactory = LoggerFactory.Create(logging =>
{
    logging.AddConsole();
    logging.SetMinimumLevel(LogLevel.Debug);
});

var channel = GrpcChannel.ForAddress("https://localhost:7026");
var invoker = channel.Intercept(new ClientLoggerInterceptor(loggerFactory));
var client = new Greeter.GreeterClient(invoker);
var reply = await client.SayHelloAsync(new HelloRequest{ Name = "Rich Intercept"});
Console.WriteLine("Greeting: " + reply.Message);
Console.WriteLine("Press any key to exit...");
Console.ReadKey();