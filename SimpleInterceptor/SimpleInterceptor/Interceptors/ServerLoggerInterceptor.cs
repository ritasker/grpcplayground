﻿using Grpc.Core;
using Grpc.Core.Interceptors;

namespace SimpleInterceptor.Interceptors;

public class ServerLoggerInterceptor : Interceptor
{
    private readonly ILogger<ServerLoggerInterceptor> logger;

    public ServerLoggerInterceptor(ILogger<ServerLoggerInterceptor> logger)
    {
        this.logger = logger;
    }

    public override async Task<TResponse> UnaryServerHandler<TRequest, TResponse>(TRequest request, ServerCallContext context,
        UnaryServerMethod<TRequest, TResponse> continuation)
    {
        logger.LogInformation($"Starting receiving call. Type: {MethodType.Unary}. Method: {context.Method}.");
        try
        {
            return await continuation(request, context);
        }
        catch (Exception ex)
        {
            logger.LogError(ex, $"Error thrown by {context.Method}.");
            throw;
        }
        finally
        {
            logger.LogInformation($"Finished processing call. Type: {MethodType.Unary}. Method: {context.Method}.");
        }
    }
}